package com.company;

import java.math.BigInteger;
import java.util.Scanner;

public class Factorial
{
    public static BigInteger factorial1()
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число, факториал которого необходимо посчитать: ");
        int n = in.nextInt();
        BigInteger ret = BigInteger.ONE;
        for (int i = 1; i <= n; ++i) ret = ret.multiply(BigInteger.valueOf(i));
        return ret;
    }
}
