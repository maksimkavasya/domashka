package com.company;

public class LuckyTickets {

        public static int Ticket () {

            int KB = 0;
            for (int i = 1000; i < 1000000; i++) {
                int i1 = i / 100000 % 10,
                        i2 = i / 10000 % 10,
                        i3 = i / 1000 % 10,
                        i4 = i / 100 % 10,
                        i5 = i / 10 % 10,
                        i6 = i / 1 % 10;
                if ((i1 + i2 + i3) == (i4 + i5 + i6)) {
                    KB++;
                }
            }
            return KB;
        }
    }

