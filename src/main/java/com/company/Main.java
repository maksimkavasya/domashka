package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Введите координаты точки А (Х1 У1), через пробел: ");
        Point3 pointA = new Point3(in.nextDouble(), in.nextDouble());
        System.out.println("Введите координаты точки B (Х1 У1), через пробел: ");
        Point3 pointB = new Point3(in.nextDouble(), in.nextDouble());
        System.out.println(String.format("Расстояние между точками: %s", Point3.distance(pointA, pointB)));
    }
}

class Fact {

    public static void main(String[] args) {

        System.out.println(String.format("Факториал равен: %s", Factorial.factorial1()));
    }
}


class LegkoBeg {

    public static void main(String[] args) {

        System.out.println(String.format("Расстояние до точки А: %s", Distance.atlet(10, 10)));
    }
}

class LuckyTicket {

    public static void main(String[] args) {

        System.out.println(String.format("Количество счастливых билетов: %s", LuckyTickets.Ticket()));
    }
}