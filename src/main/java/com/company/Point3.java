package com.company;

import java.util.Scanner;

public class Point3
{
    private double x,y;

    public Point3(double x,double y) {
        this.x = x;
        this.y = y;
    }

    public static double distance(Point3 pointA, Point3 pointB) {
        return (Math.sqrt( Math.pow(pointB.x-pointA.x,2) + Math.pow(pointB.y-pointA.y,2)));
    }
}