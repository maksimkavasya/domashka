package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LegkoBegTest {

    /**
     * Время нахождения в пути 8 секунд, путь = 10, ответ = 8
     * 30.08.2019
     * m.i.vasilyev
     */
    @Test
    public void checkPoint() {
        int distance = Distance.atlet(10, 8);
        assertEquals(8, distance, String.format("Дистанция  до точки А %d", distance));
    }

    /**
     * Время нахождения в пути 10 секунд, путь = 10, ответ = 10
     * 30.08.2019
     * m.i.vasilyev
     */
    @Test
    public void checkDistance1() {
        int distance = Distance.atlet(10, 10);
        assertEquals(10, distance, String.format("Дистанция  до точки А %d", distance));
    }

    /**
     * Время нахождения в пути 34 секунд, путь = 10, ответ = 6
     * 30.08.2019
     * m.i.vasilyev
     */
    @Test
    public void checkDistance2() {
        int distance = Distance.atlet(10, 34);
        assertEquals(6, distance, String.format("Дистанция  до точки А %d", distance));
    }

    /**
     * Время нахождения в пути 34 секунд, путь = 10, ответ = 0
     * 30.08.2019
     * m.i.vasilyev
     */
    @Test
    public void checkDistance3() {
        int distance = Distance.atlet(10, 40);
        assertEquals(0, distance, String.format("Дистанция  до точки А %d", distance));
    }

}