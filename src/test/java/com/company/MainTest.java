package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    /**
     * Расстояние между точками должно быть нуль (Проверено гуглом)
     * 30.08.2019
     * m.i.vasilyev
     */
    @Test
    public void checkDistance() {
        Point3 pointA= new Point3(10,10);
        Point3 pointB= new Point3(10,10);
        double passtoyanie = Point3.distance(pointA, pointB);
        assertEquals(0, passtoyanie, String.format("расстояние между точками: %s", passtoyanie));
    }

    /**
     * Расстояние между точками должно быть не нуль (Проверено гуглом
     * 30.08.2019
     * m.i.vasilyev
     */
    @Test
    public void checkDistance1() {
        Point3 pointA= new Point3(10,15);
        Point3 pointB= new Point3(10,25);
        double passtoyanie = Point3.distance(pointA, pointB);
        assertEquals(0, passtoyanie, String.format("расстояние между точками: %s", passtoyanie));
    }

}